# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151203222623) do

  create_table "categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "color",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "has_categories", force: :cascade do |t|
    t.integer  "proyect_id",  limit: 4
    t.integer  "category_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "has_categories", ["category_id"], name: "index_has_categories_on_category_id", using: :btree
  add_index "has_categories", ["proyect_id"], name: "index_has_categories_on_proyect_id", using: :btree

  create_table "proyect_files", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "url_img",    limit: 255
    t.string   "format_img", limit: 255
    t.integer  "proyect_id", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "proyect_files", ["proyect_id"], name: "index_proyect_files_on_proyect_id", using: :btree

  create_table "proyects", force: :cascade do |t|
    t.string   "title",              limit: 255
    t.text     "body",               limit: 65535
    t.datetime "start_date"
    t.datetime "finish_date"
    t.string   "state",              limit: 255,   default: "in_draft"
    t.integer  "user_id",            limit: 4
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "cover_file_name",    limit: 255
    t.string   "cover_content_type", limit: 255
    t.integer  "cover_file_size",    limit: 4
    t.datetime "cover_updated_at"
  end

  add_index "proyects", ["user_id"], name: "index_proyects_on_user_id", using: :btree

  create_table "responsibilities", force: :cascade do |t|
    t.integer  "proyect_id",     limit: 4
    t.integer  "responsible_id", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "responsibilities", ["proyect_id"], name: "index_responsibilities_on_proyect_id", using: :btree
  add_index "responsibilities", ["responsible_id"], name: "index_responsibilities_on_responsible_id", using: :btree

  create_table "responsibles", force: :cascade do |t|
    t.string   "first_name",         limit: 255
    t.string   "last_name",          limit: 255
    t.string   "email",              limit: 255
    t.text     "address",            limit: 65535
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "photo_file_name",    limit: 255
    t.string   "photo_content_type", limit: 255
    t.integer  "photo_file_size",    limit: 4
    t.datetime "photo_updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.integer  "permission_level",       limit: 4,   default: 1
    t.string   "nickname",               limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "has_categories", "categories"
  add_foreign_key "has_categories", "proyects"
  add_foreign_key "proyect_files", "proyects"
  add_foreign_key "proyects", "users"
  add_foreign_key "responsibilities", "proyects"
  add_foreign_key "responsibilities", "responsibles"
end
